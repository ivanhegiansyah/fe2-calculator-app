module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        "dark-theme": {
          DEFAULT: "#17171C",
        },
        "btn-dark-gray-1": {
          DEFAULT: "#4E505F",
        },
        "btn-dark-gray-2": {
          DEFAULT: "#2E2F38",
        },
        "light-theme": {
          DEFAULT: "#F1F2F3",
        },
        "btn-light-gray": {
          DEFAULT: "#D2D3DA",
        },
        "btn-blue": {
          DEFAULT: "#4B5EFC",
        },
      },
      fontFamily: {
        main: ["Work Sans"],
      },
    },
  },
  plugins: [],
};
