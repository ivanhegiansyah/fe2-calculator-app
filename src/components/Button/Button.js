const Button = ({
  text,
  className,
  iconClassName,
  onClick,
  disabled = false,
  icon = false,
}) => {
  return (
    <button
      disabled={disabled}
      className={`w-16 h-16 px-4 py-3 block text-2xl transition-colors rounded-3xl hover:shadow-lg ${className}`}
      onClick={onClick}
    >
      {text}
      {icon && <img className={`${iconClassName}`} src={icon} alt="" />}
    </button>
  );
};

export default Button;
