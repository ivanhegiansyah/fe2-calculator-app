const Label = ({ text, className }) => {
  return (
    <div
      className={`text-yellow-600 font-bold text-center px-3 py-2 rounded-lg -rotate-45 relative top-10 -left-12 w-52 ${className}`}
    >
      {text}
    </div>
  );
};
export default Label;
